#ifndef TEST3_H
#define TEST3_H
#include <QtTest>
#include"../testfunction.h"
class Test1 : public QObject
{
    Q_OBJECT

public:
    Test1();
    ~Test1();

private slots:
    void test_case1();
    void test_case2();
    void test_case3();


};
#endif // TEST3_H
