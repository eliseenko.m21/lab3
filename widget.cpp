#include "widget.h"
#include "testfunction.h"
#include <cmath>

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *buttons = new QHBoxLayout();
    buttons->addWidget(openFileButton);
    QHBoxLayout *texts = new QHBoxLayout();
    QLabel *fLabel = new QLabel("Перевести:");
    QLabel *sLabel = new QLabel("из:");
    QLabel *tLabel = new QLabel("в:");
    QLabel *lLabel = new QLabel("результат:");
    texts->addWidget(fLabel);
    texts->addWidget(inputText1);
    texts->addWidget(sLabel);
    texts->addWidget(inputText2);
    texts->addWidget(tLabel);
    texts->addWidget(inputText3);
    texts->addWidget(lLabel);
    texts->addWidget(outputText);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(buttons);
    mainLayout->addLayout(texts);
    setLayout(mainLayout);

    connect(openFileButton, SIGNAL(clicked()), this, SLOT(openFile()));
    }

void Widget::openFile()
{
    int a = 0;
    int b = 0;
    int c = 0;
    int sum = 0;

    outputText->clear();
    QString number = inputText1->text();
    QString ss1 = inputText2->text();
    QString ss2 = inputText3->text();
    a = number.split(" ")[0].toInt();
    b = ss1.split(" ")[0].toInt();
    c = ss2.split(" ")[0].toInt();
    QString s = nullptr;
    //s = convrsionHexOct(number, b, c);
    if ((b>10) || (c>10))
    {
    s = convrsionHexOct(number, b, c);
    }
    else
    {
    sum = mul(b, a);
    a = divn(c,sum);
    s = QString::number(a);
    }

    outputText->insertPlainText(s);
}


