#include "widget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.setWindowTitle("Lab 3");
    w.setFixedSize(600, 100);
    w.show();

    return a.exec();
}
