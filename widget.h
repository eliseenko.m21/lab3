#ifndef WIDGET_H
#define WIDGET_H

#include <QPushButton>
//#include <QWidget>
#include <QTextEdit>
#include <QLineEdit>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
private:
    QPushButton *openFileButton = new QPushButton("CONVERSION");
    QLineEdit *inputText1 = new QLineEdit();
    QLineEdit *inputText2 = new QLineEdit();
    QLineEdit *inputText3 = new QLineEdit();
    QTextEdit *outputText = new QTextEdit();

public slots:
    void openFile();
//    int mul(int firstSS, int inNum);
//    int div(int secondSS, int outNum);
//    QString convrsionHexOct(QString number, int firstSS, int secondSS);
};

#endif // WIDGET_H
